/*
 * GStreamer
 * Copyright (C) 2015 Frédéric Sureau <frederic.sureau@veo-labs.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-multi_sync
 *
 * Helps synchronizing multiple streams
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * GST_DEBUG=multisync:7 gst-launch-1.0 -ev multisync name=sync
 *       videotestsrc timestamp-offset=3000000000 ! sync.sink_0  sync.src_0 ! fakesink silent=false
 *       videotestsrc timestamp-offset=2000000000 ! sync.sink_1  sync.src_1 ! fakesink silent=false
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include <stdio.h>
#include "gstmultisync.h"

/**
 * Structure containing all information and properties about
 * a single stream.
 */
typedef struct _GstSingleStream GstSingleStream;

struct _GstSingleStream
{
  GstPad *sinkpad;
  GstPad *srcpad;
  GstBuffer *last_buffer;
};

GST_DEBUG_CATEGORY_STATIC (gst_multi_sync_debug);
#define GST_CAT_DEFAULT gst_multi_sync_debug

/* Signals and args */
enum
{
  LAST_SIGNAL
};

enum
{
  PROP_0
};

#define GST_MULTI_SYNC_MUTEX_LOCK(m) G_STMT_START {                          \
  g_mutex_lock (&m->lock);                                              \
} G_STMT_END

#define GST_MULTI_SYNC_MUTEX_UNLOCK(m) G_STMT_START {                        \
  g_mutex_unlock (&m->lock);                                            \
} G_STMT_END

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink_%u",
    GST_PAD_SINK,
    GST_PAD_REQUEST,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src_%u",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS_ANY);

#define gst_multi_sync_parent_class parent_class
G_DEFINE_TYPE (GstMultiSync, gst_multi_sync, GST_TYPE_ELEMENT);

static void gst_multi_sync_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_multi_sync_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static void gst_multi_sync_finalize (GObject * object);

static GstPad *gst_multi_sync_request_new_pad (GstElement * element,
    GstPadTemplate * temp, const gchar * name, const GstCaps * caps);
static void gst_multi_sync_release_pad (GstElement * element, GstPad * pad);

static GstFlowReturn gst_multi_sync_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buffer);
static gboolean gst_multi_sync_sink_event (GstPad * pad, GstObject * parent,
    GstEvent * event);

/* GObject vmethod implementations */

static void
gst_multi_sync_class_init (GstMultiSyncClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_multi_sync_set_property;
  gobject_class->get_property = gst_multi_sync_get_property;
  gobject_class->finalize = gst_multi_sync_finalize;

  gst_element_class_set_static_metadata (gstelement_class,
    "MultiSync",
    "Generic",
    "Drop buffers until all streams have data",
    "Frédéric Sureau <frederic.sureau@veo-labs.com>");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&srctemplate));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sinktemplate));

  gstelement_class->request_new_pad =
      GST_DEBUG_FUNCPTR (gst_multi_sync_request_new_pad);
  gstelement_class->release_pad =
      GST_DEBUG_FUNCPTR (gst_multi_sync_release_pad);
}

static void
gst_multi_sync_init (GstMultiSync * msync)
{
  msync->pad_id_count = 0;
  msync->start_timestamp = GST_CLOCK_TIME_NONE;
  g_mutex_init (&msync->lock);
}

static void
gst_multi_sync_finalize (GObject * object)
{
  GstMultiSync *msync = GST_MULTI_SYNC (object);

  g_mutex_clear (&msync->lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_multi_sync_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_multi_sync_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstIterator *
gst_multi_sync_iterate_internal_links (GstPad * pad, GstObject * parent)
{
  GstIterator *it = NULL;
  GstSingleStream *sstream;
  GstPad *opad;
  GValue val = { 0, };

  sstream = gst_pad_get_element_private (pad);
  if (!sstream)
    return NULL;

  if (sstream->sinkpad == pad)
    opad = sstream->srcpad;
  else if (sstream->srcpad == pad)
    opad = sstream->sinkpad;
  else
    return NULL;

  g_value_init (&val, GST_TYPE_PAD);
  g_value_set_object (&val, opad);
  it = gst_iterator_new_single (GST_TYPE_PAD, &val);
  g_value_unset (&val);

  return it;
}

/* GstElement vmethod implementations */

static GstPad *
gst_multi_sync_request_new_pad (GstElement * element, GstPadTemplate * temp,
    const gchar * name, const GstCaps * caps)
{
  guint id = -1;
  gchar *tmpname = NULL;
  GstMultiSync *msync = GST_MULTI_SYNC(element);
  GstSingleStream *sstream = NULL;

  if (name) {
    sscanf (name + 4, "_%u", &id);
    GST_LOG_OBJECT (element, "name : %s (id %d)", GST_STR_NULL (name), id);
  }

  id = (id == -1) ? msync->pad_id_count : id;
  msync->pad_id_count = MAX(msync->pad_id_count, id) + 1;

  sstream = g_new0 (GstSingleStream, 1);
  sstream->last_buffer = NULL;

  /* Create sink pad */
  tmpname = g_strdup_printf ("sink_%u", id);
  sstream->sinkpad = gst_pad_new_from_static_template (&sinktemplate, tmpname);
  g_free (tmpname);

  gst_pad_set_chain_function (sstream->sinkpad,
      GST_DEBUG_FUNCPTR (gst_multi_sync_chain));
  gst_pad_set_event_function (sstream->sinkpad,
      GST_DEBUG_FUNCPTR (gst_multi_sync_sink_event));
  gst_pad_set_iterate_internal_links_function (sstream->sinkpad,
      GST_DEBUG_FUNCPTR (gst_multi_sync_iterate_internal_links));
  GST_OBJECT_FLAG_SET (sstream->sinkpad, GST_PAD_FLAG_PROXY_CAPS);

  /* Create corresponding src pad */
  tmpname = g_strdup_printf ("src_%u", id);
  sstream->srcpad = gst_pad_new_from_static_template (&srctemplate, tmpname);
  g_free (tmpname);

  gst_pad_set_iterate_internal_links_function (sstream->srcpad,
      GST_DEBUG_FUNCPTR (gst_multi_sync_iterate_internal_links));
  GST_OBJECT_FLAG_SET (sstream->srcpad, GST_PAD_FLAG_PROXY_CAPS);

  gst_pad_set_element_private (sstream->sinkpad, (gpointer) sstream);
  gst_pad_set_element_private (sstream->srcpad, (gpointer) sstream);

  /* only activate the pads when we are not in the NULL state
   * and add the pad under the state_lock to prevend state changes
   * between activating and adding */
  g_rec_mutex_lock (GST_STATE_GET_LOCK (element));
  if (GST_STATE_TARGET (element) != GST_STATE_NULL) {
    gst_pad_set_active (sstream->srcpad, TRUE);
    gst_pad_set_active (sstream->sinkpad, TRUE);
  }
  gst_element_add_pad (element, sstream->srcpad);
  gst_element_add_pad (element, sstream->sinkpad);
  g_rec_mutex_unlock (GST_STATE_GET_LOCK (element));

  GST_DEBUG_OBJECT (element, "Added source pad %s:%s",
      GST_DEBUG_PAD_NAME (sstream->srcpad));

  GST_DEBUG_OBJECT (element, "Returning pad %s:%s",
      GST_DEBUG_PAD_NAME (sstream->sinkpad));

  return sstream->sinkpad;
}

static void
gst_multi_sync_release_pad (GstElement * element, GstPad * pad)
{
  GstSingleStream *sstream;

  GST_LOG_OBJECT (element, "pad %s:%s", GST_DEBUG_PAD_NAME (pad));

  sstream = gst_pad_get_element_private (pad);

  gst_pad_set_active (sstream->srcpad, FALSE);
  gst_pad_set_active (sstream->sinkpad, FALSE);
  gst_pad_set_element_private (sstream->srcpad, NULL);
  gst_pad_set_element_private (sstream->sinkpad, NULL);
  gst_element_remove_pad (element, sstream->srcpad);
  gst_element_remove_pad (element, sstream->sinkpad);
  
  if (sstream->last_buffer) {
    gst_buffer_unref (sstream->last_buffer);
  }
  g_free(sstream);
}

static GstClockTime
gst_multi_sync_find_start_timestamp (GstMultiSync * msync)
{
  GstIterator *it;
  GValue item = G_VALUE_INIT;
  gboolean done;
  GstClockTime start_timestamp = GST_CLOCK_TIME_NONE;

  it = gst_element_iterate_sink_pads (GST_ELEMENT (msync));
  done = FALSE;
  while (!done) {
    switch (gst_iterator_next (it, &item)) {
      case GST_ITERATOR_OK:
      {
        GstPad *pad;
        GstSingleStream *sstream;
        GstClockTime current_timestamp;

        pad = g_value_get_object (&item);
        sstream = gst_pad_get_element_private (pad);

        if (sstream->last_buffer == NULL) {
          start_timestamp = GST_CLOCK_TIME_NONE;
          done = TRUE;
          break;
        }

        current_timestamp = GST_BUFFER_PTS (sstream->last_buffer);
        if (GST_CLOCK_TIME_IS_VALID (start_timestamp))
          start_timestamp = MAX (start_timestamp, current_timestamp);
        else
          start_timestamp = current_timestamp;

        g_value_reset (&item);
        break;
      }
      case GST_ITERATOR_RESYNC:
        gst_iterator_resync (it);
        break;
      case GST_ITERATOR_ERROR:
        GST_ERROR_OBJECT (msync,
            "Could not iterate over sink pads");
        done = TRUE;
        break;
      case GST_ITERATOR_DONE:
        done = TRUE;
        break;
    }
  }
  g_value_unset (&item);
  gst_iterator_free (it);
  
  return start_timestamp;
}

static GstFlowReturn
gst_multi_sync_push_buffer (GstPad * pad, GstObject * parent, GstBuffer * buffer)
{
  GstMultiSync *msync;
  
  msync = GST_MULTI_SYNC (parent);
  
  GST_DEBUG_OBJECT (pad, "Current buffer TS is: %" GST_TIME_FORMAT, GST_TIME_ARGS (GST_BUFFER_PTS (buffer)));

  if (GST_BUFFER_PTS (buffer) < msync->start_timestamp) {
    GST_DEBUG_OBJECT (pad, "Before start: dropping");
    gst_buffer_unref (buffer);
    return GST_FLOW_OK;
  }

  GST_BUFFER_PTS (buffer) -= msync->start_timestamp;
  GST_DEBUG_OBJECT (pad, "New TS %" GST_TIME_FORMAT, GST_TIME_ARGS (GST_BUFFER_PTS (buffer)));
  return gst_pad_push (pad, buffer);
}

/* TODO handle EOS */
static GstFlowReturn
gst_multi_sync_chain (GstPad * pad, GstObject * parent, GstBuffer * buffer)
{
  GstSingleStream *sstream;
  GstMultiSync *msync;

  sstream = gst_pad_get_element_private (pad);
  msync = GST_MULTI_SYNC (parent);

  GST_MULTI_SYNC_MUTEX_LOCK(msync);

  if (!GST_CLOCK_TIME_IS_VALID (msync->start_timestamp)) {

    GST_DEBUG_OBJECT (pad, "No start TS yet");
    GST_DEBUG_OBJECT (pad, "Last buffer was %p", sstream->last_buffer);
    GST_DEBUG_OBJECT (pad, "New buffer is %p", buffer);

    /* replace last buffer */  
    if (sstream->last_buffer)
      gst_buffer_unref (sstream->last_buffer);

    sstream->last_buffer = buffer;

    GST_DEBUG_OBJECT (pad, "Trying to determine start TS");
    msync->start_timestamp = gst_multi_sync_find_start_timestamp (msync);
    if (!GST_CLOCK_TIME_IS_VALID (msync->start_timestamp)) {
      GST_DEBUG_OBJECT (pad, "Start TS not found");
      GST_MULTI_SYNC_MUTEX_UNLOCK(msync);
      return GST_FLOW_OK;
    }

    GST_DEBUG_OBJECT (pad, "Start TS found: %" GST_TIME_FORMAT, GST_TIME_ARGS (msync->start_timestamp));
    sstream->last_buffer = NULL;
  }

  GST_MULTI_SYNC_MUTEX_UNLOCK(msync);

  if (sstream->last_buffer) {
    GST_DEBUG_OBJECT (pad, "Pushing waiting buffer %p", sstream->last_buffer);
    gst_multi_sync_push_buffer (sstream->srcpad, parent, sstream->last_buffer);
    sstream->last_buffer = NULL;
  }
  
  return gst_multi_sync_push_buffer (sstream->srcpad, parent, buffer);
}

static gboolean
gst_multi_sync_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  GstSingleStream *sstream;
  gboolean res;

  sstream = gst_pad_get_element_private (pad);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
      res = gst_pad_push_event (sstream->srcpad, event);
      break;

    default:
      res = gst_pad_event_default (pad, parent, event);
      break;
  }

  return res;
}

static gboolean
multi_sync_init (GstPlugin * multisync)
{
  GST_DEBUG_CATEGORY_INIT (gst_multi_sync_debug, "multisync",
      0, "Multiple streams sync");

  return gst_element_register (multisync, "multisync", GST_RANK_NONE,
      GST_TYPE_MULTI_SYNC);
}

#ifndef PACKAGE
#define PACKAGE "multisync"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    multi_sync,
    "Multiple streams sync",
    multi_sync_init,
    VERSION,
    "LGPL",
    "GStreamer",
    "http://gstreamer.net/"
)
